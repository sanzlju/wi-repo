#
##	COMPARE FILES FROM GIT WITH THE FILES DEPLOYED IN ENV.
#

#TEST ENV DIR
#FRONT_END_DIR=//wltrtd03.cc.cec.eu.int/participant_portal_4_tst/ar/

#PROD ENV DIR
FRONT_END_DIR=//rtdcogep.cc.cec.eu.int/participant_portal_4_prod/ar/

#FOR TESTING
#FRONT_END_DIR=c:/!tmp/0

echo "Pulling the latest from git..."
git pull

echo ""

##########COMPARES ONLY JS AND CSS#########
diff -wqr ./input/research/content/js/ $FRONT_END_DIR/input/research/content/js/
diff -wqr ./input/research/content/css/ $FRONT_END_DIR/input/research/content/css/