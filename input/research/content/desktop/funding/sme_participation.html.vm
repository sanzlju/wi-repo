#parse("init.vm")
## -----------------------------------------------------
## - $visibility : page visibility : public | private | admin
## - $page : name of your html page
## - $menu : menu to highlight
## - $breadcrumb : last element in the breadcrumb
## - $title, $keywords, $description : html metadatas
## -----------------------------------------------------
#set($visibility = "public")
#set($page = "funding/sme_participation.html")
#set($menu = "funding")
#set($breadcrumb = "Organisation Search")
#set($title= $text.organisations.search.meta.title)
## -----------------------------------------------------
#parse("header.vm")
<div class="row">

	<div class="secure span3 span3-top">#parse("htp_menu.vm")</div>
	<div class="anonymous span3">#parse("htp_menu.vm")</div>

		<!-- start content -->
		<div class="span9">
			<div>
				<div class="fragment" data-url="${root}/../data/fragments/desktop/en/all.html" data-options=""></div>
				<div class="fragment" data-url="${root}/../data/fragments/desktop/en/$page" data-options=""></div>
			</div>
			<h4>SME Participation <a href="#" data-url="$link-funding-guide-sme" class="btn btn-grey btn-small pull-right wizard700x1000">H2020 online manual</a></h4>
			<h6>SME Instrument</h6>
			<p>The SME instrument has been designed specifically for <strong>single or groups of highly innovative SMEs</strong>
			with international ambitions, determined to turn strong, innovative business ideas into winners on the market.
			The instrument provides <strong>full-cycle business innovation support</strong> from the stage of business idea conception and
			planning (phase I) over business plan execution and demonstration (phase II) to commercialisation (phase III).
			Participants will be able to call on business innovation coaching for the duration of their project.
			<ul>
				<li><strong>Phase I (proof-of-concept):</strong> Explore the scientific or technical feasibility and commercial potential
				of your new idea in order to develop an innovation project, with the help of a &#8364; 50,000 grant, and receive more support in case of a positive outcome!
				</li>
				<li><strong>Phase II (development & demonstration):</strong> Develop your sound, ground-breaking business idea further with the help
				of a grant in the order of &#8364; 500,000 to 2,5 million into a market-ready product, service or process!
				</li>
				<li><strong>Phase III (go-to-market):</strong> Take advantage of additional EU support to enter the market successfully (no grants).</li>
			</ul>

			<p>
					For more information about the SME instrument, please see the <a href="http://ec.europa.eu/research/participants/portal/desktop/en/support/faq.html#c,faqs=categoryMachineName/t/pp_roles_and_rights/0/1/1&categoryMachineName/t/p_registration/0/1/1&categoryMachineName/t/work_programme_calls/0/1/1&categoryMachineName/t/p_submission_eval/0/1/1&categoryMachineName/t/ethics_research_integrity/0/1/1&categoryMachineName/t/grants/0/1/1&categoryMachineName/t/audit/0/1/1&categoryMachineName/t/experts/0/1/1&categoryMachineName/t/eu_research_policy/0/1/1&programmeList/t/FP7/1/1/0&programmeList/t/RFCS/1/1/0&programmeList/t/Consumer%20Programme/1/1/0&programmeList/t/3rd%20Health%20Programme/1/1/0&programmeList/t/COSME/1/1/0&programmeList/t/Justice%20Programme/1/1/0&programmeList/t/H2020/1/1/0&programmeList/t/EAC/1/1/0&question,answer,category,tagList,programmeList/s/SME%20instrument/1/1/0&+undefined/undefined" target="_blank">related FAQ</a>.
			</p>
			<p class="text-right"><a href="$root/desktop/en/opportunities/h2020/ftags/sme_instrument.html#c,topics=flags/s/SME/1/1&+callIdentifier/desc" class="btn btn-small btn-primary">Apply</a></p>

			<h6>Collaborative Projects (SME + partners)</h6>
			<p>Work with at least 2 international <strong>partners</strong> (separate legal entities) from different EU countries (at least 3 in total) and request EU project support! To find partners, you can use the assistance of various <a href="#" data-url="${link-funding-guide-find-partners}" class="wizard700x1000">partner search services</a>.</p>
			<p class="text-right"><a href="$root/desktop/en/opportunities/h2020/search/search_topics.html#c,topics=topicFileName,callIdentifier,callTitle,identifier,title,description,tags,flags/s/SME/1/1/0&+title/desc" class="btn btn-small btn-primary">Apply</a></p>

			<h6>Access to Debt and Equity Financing</h6>
			<p>Discover where and how the EU can help your company to <strong>access finance</strong> for further business development.</p>
			<p>For more information, please see the Work Programme part <a href="http://ec.europa.eu/research/participants/portal/doc/call/h2020/common/1587761-06._accesstoriskfinance_wp2014-2015_en.pdf" target="_blank">Access to Risk Finance.</a></p>
			<p>You find additional guidance on how to apply for finance supported by EU on the <a href="http://www.access2finance.eu/" target="_blank">Access to Finance web portal</a>. It also helps you to locate banks or venture capital funds that provide such finance in your country. </p>

			<br/>

			<div class="row">
				<div class="span9">
					<div class="well">
						<h3>Support to SME</h3>
						<p>Get personalized advice and guidance on how to apply from two support networks who understand your local and global challenges.
						Discover your personal help lines:</p>
						<h4><a href="$langpath/support/enn.html">Enterprise Europe Network</a></h4>
						<p>Whether you need information on EU legislation, help finding a business partner, want to benefit from innovation networks
						in your region or need information on funding opportunities, this is the place to start.</p>
						<h4><a href="../support/national_contact_points.html">National Contact Points (NCPs)</a></h4>
						<p>NCPs provide information and guidance to SMEs wishing to participate in EU research and are able to offer personalized support
						in the proposer's own language.</p>
						<h4><a href="$langpath/funding/guide.html">H2020 online manual</a></h4>
						<p>Quick online guide on steps from proposal preparation up to reporting on your project.</p>
						<h4><a href="http://www.iprhelpdesk.eu/SME_Corner">IPR (SME corner)</a></h4>
						<p>Useful information for managing intellectual property rights in your business and projects.</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="span9">
					<div class="well">
						<h3>Is my Company an SME ?</h3>
						<p>
						     To count as an SME, your organisation must be engaged in an economic activity and must have:
						     <ul>
						         <li> fewer than 250 employees <br> and </li>
						         <li> an annual turnover of <b>no more</b> than &#8364;50 million and/or an annual balance sheet of <b>no more</b> than &#8364;43 million.</li>
						     </ul>
						</p>
						<p>
							Whether you count as an SME may depend on how you count your workforce, turnover or balance sheet. Please note that you must take account of any relationships you have with other enterprises. Depending on the category in which your enterprise fits you may then need to add some, or all of the data.
							For more details: <a href="http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2003:124:0036:0041:EN:PDF" target="_blank">Recommendation 2003/361/EC</a> - see "Annex" for a full definition of an SME.
						</p>
						<p>
							The <a class=""  href="${langpath}/organisations/register.html">Beneficiary Register</a> on the portal includes a questionnaire that allows determining whether an organisation is an SME according to the EC rules and thus whether it is eligible to apply for the funding of certain H2020 actions.
						</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="span9">
					<div class="well">
						<h3>Policy Questions</h3>
						<p><a href="http://ec.europa.eu/research/sme-techweb/index_en.cfm" target="_blanc">Horizon 2020 - SME Techweb</a></p>
						<p>Stay up-to-date with latest EU policy initiatives in the field of innovation in SMEs.
						Workshops, call news and more.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- end content -->

</div>
#parse("footer.vm")