// ------------------------------------------ static -----------------------------------------------

$.support.cors = true;
var STATUS = {YES: "pong", NO: "ping", KO: "ko"};
var SCOPE = {PUBLIC: "public", PRIVATE: "private", ADMIN : "admin"};
var login;

//------------------------------------------- before -----------------------------------------------

// control storage
var site = controlStorage();

// control user
var user = controlUser();

// control access
controlAccess();
		
//------------------------------------------- after -----------------------------------------------

$(function(){


    // control display
    controlDisplay();
	
    // control dynamic content
    controlDynamic();
	
	// control event
    controlEvent();
	
	
});		
		
// ------------------------------------------ main operations -----------------------------------------------

// control storage
function controlStorage() {

	var store = $("meta[name=portal-site]").attr("content");

	if (!$.localStorage(store)) {

		var END = document.URL.indexOf("desktop");
		var ROOT = document.URL.substring(0, END);
		var DESKTOP = document.URL.substring(0, END + 11);
		var REST = "/research/participants/api/";
		var DATA = $.parseJSON('{"root":"' + ROOT + '","desktop":"' + DESKTOP + '","rest":"' + REST + '"}');
		$.localStorage(store, DATA);

	}

	return $.localStorage(store);

}

// control user
function controlUser() {

	if (isLoggedIn() == STATUS.YES) {

		if ($.sessionStorage("user") == null) {

			$.ajax({
			//async: false,
			url: site.rest + "authentication/userdetails.json",
			timeout:3000,
			dataType:'json',
			cache: false,
			crossDomain: true,
			xhrFields : {withCredentials: true},
			success: function(data){$.sessionStorage("user", data);}
			});

		}

		return $.sessionStorage("user");
		
	}

}

// control access
function controlAccess() {

	// 1 - logged
	if (isLoggedIn() == STATUS.YES) {
	
	    // 1.1 - http -> https
		if (!isHTTPS()) {
		
			redirToHTTPS();

		// 1.2 - check admin scope	
		} else {
		
			var scope = $("meta[name=portal-scope]").attr("content");
			if (scope == SCOPE.ADMIN && !user.isAdmin) {$(location).attr('href', site.desktop + "home.html");}

		}
	
	// 2 - anonymous	
	} else {

		// 2.1 - https -> http
		if (isHTTPS()) {

			redirToHTTP();

		// 2.2 - check public scope
		} else {
		
			var scope = $("meta[name=portal-scope]").attr("content");
			if (scope != SCOPE.PUBLIC) {logMeIn();}

		}

	}
	
}

// control display
function controlDisplay() {

	if (isLoggedIn() == STATUS.YES) {

		$(".pp-username").html(user.firstName + " " + user.lastName);
		$("body").addClass("secure");
		
		if (user.isAdmin) {
		
			$("body").addClass("admin");
			
			if ($.sessionStorage("profiler")) {
			
				$("#login-icon").removeClass("icon-user").addClass("admin-spy");
				$(".pp-username").html($.sessionStorage("profiler") + "<span class='profiler'>profiler</span>");
			
			}
			
		}
		
	} else {

		$("body").addClass("anonymous");

	}
	
}

// control dynamic
function controlDynamic() {

	// loop on dynamic divs
	$('.dynamic').each(function(index) {

		var proceed = true;
		var secure = false;

		if ($(this).hasClass('secure')) {secure = true;}

		// anonymous - secure content -> no proceed || logged - anonymous content -> no proceed
		if ( (secure && (isLoggedIn() != STATUS.YES)) || ($(this).hasClass('anonymous') && (isLoggedIn() == STATUS.YES)) ) {proceed = false;}

		if (proceed) {

			var fragment = $(this).attr('id');
			
			// ajax loading
			loading($(this));

			// parameters
			data = location.search.substring(1);

			// check if impersonified
			var profiler = $.sessionStorage("profiler");
			
			if (profiler != null) {
			
				if (data != "") {data += "&";}
				data += "uid=" + profiler;
			
			}

			// load fragment
			if (data != "") {
			
				$(this).load(site.rest + fragment, data, function(response, status, xhr) {FEF.components.initiateTables();if (status == "error") {error($(this));}});

			} else {

				$(this).load(site.rest + fragment, function(response, status, xhr) {FEF.components.initiateTables();if (status == "error") {error($(this));}});

			}
				
		}

	});

}

// control event
function controlEvent() {

	// login event
	$(".pp-login").click(function(event) {

		event.preventDefault();

		if (isLoggedIn(true) != STATUS.KO) {

			// ecas login -> redirect to current page
			logMeIn();
			
		} else {
		
			error("sorry");
			
		}

	});
	
	// logout event
	$(".pp-logout").click(function(event) {

		event.preventDefault();

		if (isLoggedIn(true)  != STATUS.KO) {

			// delete user data
			$.sessionStorage("user", null);
			$.sessionStorage("profiler", null);
			user = null;
			login = null;
		
			// ecas logout -> redirect to current page
			logMeOut();
			
		} else {
		
			error("sorry");
			
		}

	});
	
	// forward link with parameter
	$("a[data-param]").click(function(event) {

		event.preventDefault();
		var keys = $(this).attr("data-param").split(",");
		var url = $(this).attr("href") + "?";
		$.each(keys, function(index, key) { url += key + "=" + getParamValue(key) + "&";});
		$(location).attr("href", url.slice(0,-1));
		
	});	
}

//------------------------------------------ util operations -----------------------------------------------

// dot trick
function format(fragment) {return fragment.replace('.','\\.').replace('/','\\/');}

// redirect to https
function redirToHTTPS() {if (location.href.indexOf("https://") == -1) {location.href = location.href.replace("http://", "https://");}}

// redirect to http
function redirToHTTP() {if (location.href.indexOf("http://") == -1) {location.href = location.href.replace("https://", "http://");}}

// check if https
function isHTTPS() {return window.location.protocol == "https:";}

// log in
function logMeIn() {$(location).attr("href", site.rest + "authentication/login.html?from=" + document.URL);}

// log out
function logMeOut() {$(location).attr('href',site.rest + "authentication/logout.html?service=" + site.desktop + "home.html");}

// url param
function getParamValue(key) {key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));return match && decodeURIComponent(match[1].replace(/\+/g, " "));}

// ajax loading
function loading(element) {element.html("<img class='loader' src='" + site.root + "img/general/ajax-loader.gif'/>");return;}

// check the login status
function isLoggedIn(force) {

	if (login == null || force) {

		$.ajax({
		//async: false,
		url: site.rest + "authentication/status.json",
		timeout:3000,
		dataType:'json',
		cache: false,
		crossDomain: true,
		xhrFields : {withCredentials: true},
		success: function(data){login = data.loginStatus;},
		error: function(){login = STATUS.KO}});
		
	}

	return login;

}

// error handler
function error(element) {

	if (element == "sorry") {

		$.ajax({url: site.desktop + "fragment/sorry.html", success: function (data) {$('.alert-error').remove();$('#content').prepend(data); $('#sorry').slideDown(); $('#sorry').delay(7000).fadeOut(1000);}, dataType: 'html'});

	} else {

		element.load(site.desktop + "fragment/unavailable.html");

	}
	
}

//----------------------------------------------------------------------------------------------------------