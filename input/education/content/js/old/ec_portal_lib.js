var EC_PORTAL = EC_PORTAL || {};
var EC_PORTAL =  {PROTOCOL: {HTTPS_SCHEME:"https:",	HTTP_SCHEME:"http:"},
		          BROWSING_SCOPE:{PUBLIC: 'public',PRIVATE:'private'},
		          LOGIN_STATUS: {SUCCESS: "pong",FAILED: "ping",UNAVAILABLE: "unavailable"},
		          LOGIN_STATUS_LOCAL : {NOT_LOGGED_IN:-1,UNKNOWN: 0,LOGGED_IN:1},
		          ENDPOINTS : {
					   LOGIN_ENDPOINT:"/research/participants/api/authentication/login.html",
					   LOGIN_STATUS_ENDPOINT:"/research/participants/api/authentication/status.json",
					   USER_DETAILS_ENDPOINT:"/research/participants/api/authentication/userdetails.json"
				},
				ERRNO:{SERVICE_UNAVAILABLE:"err_service_unavailable"},
				COOKIE_PATH: "/"
	};
EC_PORTAL.namespace = function (ns_string) {
    var parts = ns_string.split('.'),
        parent = EC_PORTAL,
        i;
    // strip redundant leading global
    if (parts[0] === "EC_PORTAL") {
        parts = parts.slice(1);
    }
    for (i = 0; i < parts.length; i += 1) {
        // create a property if it doesn't exist
        if (typeof parent[parts[i]] === "undefined") {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    }
    return parent;
};

/*******************************************************************************************************/
/*  Utilities functions
/*******************************************************************************************************/
EC_PORTAL.namespace('EC_PORTAL.utils');
 
EC_PORTAL.utils = (function() {
		return {
			getCurrentURL : function() {
				return document.URL;
			},
			redirect : function(url) {
				window.location.href=url;
			},
			redirectCurrentURLToSSL : function() {
				if(this.isHttpScheme()) { // fine tuning to be done
					ssl_url = this.httpToHttps();
					this.redirect(ssl_url);
				}
			},
			reload: function() {
				window.location.reload(true);
			},
			httpToHttps : function() {
				return EC_PORTAL.PROTOCOL.HTTPS_SCHEME + "//" + this.getHost() +this.getCurrentPath();
			},
			getScheme : function() {
				return window.location.protocol;
			},
			getHost : function() {

				return window.location.host;
			},
			getCurrentPath : function() {
				return window.location.pathname;
			},
			isHttpsScheme : function() {
				return EC_PORTAL.PROTOCOL.HTTPS_SCHEME == EC_PORTAL_UTILS.getScheme();
			},
			isHttpScheme : function(){
				return EC_PORTAL.PROTOCOL.HTTP_SCHEME == EC_PORTAL_UTILS.getScheme();
			},
			isProtocolHTTP: function() {
				return this.isHttpScheme();
			},
			writeCookieHttps: function(name,value) {
				$.cookie(name,value,{ path:EC_PORTAL.COOKIE_PATH,secure : true });
			},
			getCookieValue: function(name){
				var cookieValue = $.cookie(name);
				if(cookieValue === undefined || cookieValue == "null") {
					cookieValue = null;
				}
				return cookieValue;
			},
			writeCookie : function(name,value,expire){
				$.cookie(name,value,{path:EC_PORTAL.COOKIE_PATH,expires : expire });
			},
			removeCookie: function(name) {
				var isRemoved = false;
				if(this.getCookieValue() != null) {
					$.cookie(name,null,{path:EC_PORTAL.COOKIE_PATH,expires:-5});
					isRemoved = true;
				}
				return isRemoved;
			},
			removeCookieHttps: function(name) {
				var isRemoved = false;
				if(this.getCookieValue() != null) {
					$.cookie(name,null,{secure:true,path:EC_PORTAL.COOKIE_PATH,expires:-5});
					isRemoved = true;
				}
				return isRemoved;
			},
			getBrowsingScope: function() {
				// get the scope from the page
				// kd read from a meta tag instead of a span.
				browsingScopeTxt = $('head meta[name=portal-scope]').attr('content')

				// browsingScopeTxt = $("#scope").text();
				if(browsingScopeTxt === undefined) {
					browsingScopeTxt = null;
				}
				return browsingScopeTxt;
			},
			getParameterByName: function(name) {
			    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
			    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
			},
			isValidParam: function(ridVal) {
				var ridValRegexp = new RegExp("^([a-z0-9]{2,11})$");
				var match =(ridValRegexp.exec(ridVal));
				return (match != null && match instanceof Array && match.length==2 && ridVal === match[0]);
			},
			getHashTag: function() {
				return window.location.hash;
			},
			getHashTagValue: function(param) {
				hashtag = this.getHashTag()
				hashVal = null;
				var myregexp = new RegExp("^#[a-z]{3}-([a-z0-9]{6,11}$)");
				var match =(myregexp.exec(hashtag));
				if(match != null && match instanceof Array && match.length==2) {
					hashVal =  match[1];
				}
				return hashVal;
			//	return (hashtag !== undefined && hashtag != null && hashtag.indexOf("#"+param+"-") != -1)?hashtag.substr(param.length +2):null;
			},
			log: function(text){
				window.console && console.log(text);
			},
			getInternetExplorerVersion: function() {
				var rv = -1; // Return value assumes failure.
				  if (navigator.appName == 'Microsoft Internet Explorer')
				  {
				    var ua = navigator.userAgent;
				    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
				    if (re.exec(ua) != null)
				      rv = parseFloat( RegExp.$1 );
				  }
  				return rv;
			}

	};
})();

// alias
var EC_PORTAL_UTILS = EC_PORTAL.utils;


/*******************************************************************************************************/
/*  I18n                                                                                               */
/*******************************************************************************************************/
EC_PORTAL.i18n = (function() {
	var langs =['en','fr'];
	var EN = langs[0];
	var FR = langs[1];

	var translations =[];

	translations['en'] = [];
	translations['en'][EC_PORTAL.ERRNO.SERVICE_UNAVAILABLE] = "The service is unavalaible.Please retry later";

	translations['fr'] = [];
	translations['fr'][EC_PORTAL.ERRNO.SERVICE_UNAVAILABLE] = "Le service est momentament indisponible.Veuillez re-essayer ulterieurement";
	return {
		errMessages: function(errno) {
			default_lang = EN;
			err_messages = "";
			lang = this.getLang();
			if(lang in translations && errno in translations[lang]){
				err_messages = translations[lang][errno];
			}
			else{
				err_messages = translations[default_lang][errno];
			}
			return err_messages;

		},
		getLang: function() {
			lang = EC_PORTAL.utils.getParameterByName(this.getLangParameterName());

			if(lang == null) {
				 var match = RegExp('/desktop/([a-z]{2})/').exec(window.location.pathname);
				 lang = match && match[1];
			}
			return lang;
		},
		getLangParameterName:function(){
			return "lang";
		},
		getTranslations:function() {
			return translations;
		}

	};

})();

/*******************************************************************************************************/
/*  login helpers functions                                                                            */
/*******************************************************************************************************/
EC_PORTAL.namespace('EC_PORTAL.authn');

EC_PORTAL.authn = (function(window,$) {
	// global var
	loggedIn = false;
	serviceAvailable = false;
	user = {userLoggedIn: false,
				firstName : "",
				lastName:"",
				uid:null,
				isAdmin:false,
					isLoggedIn: function() {
						return this.userLoggedIn;
					},
					setLoginStatus: function(loginState) {
						this.userLoggedIn = loginState;
					}
		};
	return {
		/* login action redirect */
		login: function(pageRef, loginEndPoint) {
			if(pageRef === undefined || pageRef == null) {
				pageRef = EC_PORTAL_UTILS.getCurrentURL();
				if(EC_PORTAL_UTILS.isHttpScheme()) {
					pageRef = EC_PORTAL_UTILS.httpToHttps();
				}
			}
			if(loginEndPoint == null) {
				loginEndPoint = "https://" + EC_PORTAL_UTILS.getHost() + EC_PORTAL.ENDPOINTS.LOGIN_ENDPOINT;
			}
			url = loginEndPoint + "?from="+pageRef;
			this.writeLocalLoginState(EC_PORTAL.LOGIN_STATUS_LOCAL.LOGGED_IN);
			EC_PORTAL_UTILS.redirect(url);
		},
		isServiceAvailable:function() {
			var loginStatus = this.loginStatus();
			// shorttcut to set the authentication status
			this.loggedIn = (EC_PORTAL.LOGIN_STATUS.SUCCESS === loginStatus);
			return this.serviceAvailable || EC_PORTAL.LOGIN_STATUS.UNAVAILABLE !==  loginStatus;
		},
		isRemoteLoggedIn: function() {
			return 	(this.loggedIn = (EC_PORTAL.LOGIN_STATUS.SUCCESS == this.loginStatus()));
		},
		/* get login status */
		isLoggedIn: function(browsingScope) {
			return this.loggedIn === true ||  this.isRemoteLoggedIn();
		},
		/* get login status */
		isUserLoggedIn: function() {
			return this.loggedIn === true||  (this.loggedIn = (EC_PORTAL.LOGIN_STATUS.SUCCESS == this.loginStatus()));
		},
		isUserAdmin: function() {
			return user.isAdmin;
		},
		initUser: function() {
			return this.getUserDetailsRemote();
		},
		getUser: function() {
			if(user.uid === undefined || user.uid == null) {
				user = this.initUser();
			}
			user.setLoginStatus(this.isUserLoggedIn());
			return user;
		},
		// get the login status
		loginStatus: function(){
			var loginStatus = EC_PORTAL.LOGIN_STATUS.FAILED;
			var ajax;
			if(ajax) {
				ajax.abort();
			}
			var currentScheme = EC_PORTAL_UTILS.getScheme();
			ajax = $.ajax({
					url:EC_PORTAL.ENDPOINTS.LOGIN_STATUS_ENDPOINT,
					async: false,
					method: 'GET',
					timeout:3000,
					dataType:'json',
					crossDomain: true,
					xhrFields : {
					  withCredentials: true
					} ,
					cache: false,
					success: function(data){
						loginStatus = data.loginStatus;
						window.console && console.log("login status:" + data.loginStatus);
						this.serviceAvailable = true;
					},
					error : function(x,t,m){
						EC_PORTAL_UTILS.log("login status:" + m);
						this.serviceAvailable = false;
						loginStatus = EC_PORTAL.LOGIN_STATUS.UNAVAILABLE;
					}
				});
			return loginStatus;
		},
		/* Local login assumptions
        /*************************/
		getLocalCurrentLoginState: function() {
			var localLoginState = $.cookie("ec_status_local");
			if(localLoginState === undefined || localLoginState == null){
				localLoginState = EC_PORTAL.LOGIN_STATUS_LOCAL.UNKNOWN;
			}
			return localLoginState;
		},
		writeLocalLoginState: function(localLoginState){
			EC_PORTAL_UTILS.writeCookie("ec_status_local",localLoginState);
		},
		writeLocalLoginStateNotLoggedIn: function(){
			writeLocalLoginState(EC_PORTAL.LOGIN_STATUS_LOCAL.NOT_LOGGED_IN);
		},

		writeLocalLoginStateLoggedIn: function(localLoginState){
			writeLocalLoginState(EC_PORTAL.LOGIN_STATUS_LOCAL.LOGGED_IN);
		},
		isLocalLoginStateUnknown:function(localLoginState){
			return this.getLocalCurrentLoginState() == EC_PORTAL.LOGIN_STATUS_LOCAL.UNKNOWN;
		},
		narrowLocalUnknownLoginState:function(browsingScope) {
		// set public as default
			var localLoginState = EC_PORTAL.LOGIN_STATUS_LOCAL.NOT_LOGGED_IN;
			switch(browsingScope) {
			case EC_PORTAL.BROWSING_SCOPE.PRIVATE:
				localLoginState = EC_PORTAL.LOGIN_STATUS_LOCAL.LOGGED_IN;
			break;
			// other cases ...
			}
			this.writeLocalLoginState(localLoginState);
			return localLoginState;
		},
		/* User details related methods
		/*****************************/
		displayUserDetails: function(browsingScope) {
			if(this.isLoggedIn()) {
				//userDetailsCook = this.getUserDetailsLocal();

				//if((userDetailsCook == undefined || userDetailsCook == null) && this.isLoggedIn()) {
				userDetails = this.getUserDetailsRemote();
				//	this.writeUserDetails(userDetails.firstName + " " + userDetails.lastName);
				EC_PORTAL_UTILS.log(userDetails);
				//}
				//else {
				//	userDetails = userDetailsCook;
					//}
				$("#pp-user_details").html(userDetails.firstName + " " + userDetails.lastName);

			}
		},
		/* get user detail from local store (cookie) */
		getUserDetailsLocal: function() {
			var userDetails = $.cookie("user_details")
			if(userDetails === undefined) {
				userDetails = null;
			}
			return userDetails;
		},
		/* get user detail from remote store */
		getUserDetailsRemote: function() {
			var  userDetails = null;
			var ajax;
			if(ajax) {
				ajax.abort();
			}
			var currentScheme = EC_PORTAL_UTILS.getScheme();
			ajax = $.ajax({
					//url:currentScheme + "//" + EC_PORTAL.LOGIN_BASE_FQDN + EC_PORTAL.ENDPOINTS.USER_DETAILS_ENDPOINT,
					url:EC_PORTAL.ENDPOINTS.USER_DETAILS_ENDPOINT,
				async:false,
				method: 'GET',
				dataType:'json',
				crossDomain: false,
				cache: false,
				xhrFields : {
				  withCredentials: true
				} ,
				success: function(data){
					userDetails = data.firstName + " " + data.lastName;
										user.lastName = data.lastName;
										user.firstName = data.firstName;
										user.isAdmin = (data.isAdmin !== undefined && data.isAdmin !== null && data.isAdmin === true);
										user.uid = data.uid;

				},
				error : function(xhr,status,error){
					if(status==="timeout" || xhr.status == "404") {

						$(panel_id_pointer).html('<span class="error">'+EC_PORTAL.i18n.errMessages(EC_PORTAL.ERRNO.service_unavailable)+'</span>');
					}
					else {
						$(panel_id_pointer).html('<span class="error">An error occured</span>'+xhr.responseText);
					}
				}
			});
			return user;
		},
		writeUserDetails:function(userDetails) {
			//$.cookie("user_details",userDetails,{ expires : 30});
			EC_PORTAL_UTILS.writeCookie("user_details",userDetails,30);
		},
		setUserOnBehalf: function(onBehalfUser){

			EC_PORTAL_UTILS.writeCookieHttps("pp_onbehalf",onBehalfUser);
		},
		getUserOnBehalf:function() {
			var cookValue = EC_PORTAL_UTILS.getCookieValue("pp_onbehalf");

			if(cookValue === undefined) {
				cookValue = null;
			}
			return cookValue;
		},
		removeUserOnBehalf:function() {
			return EC_PORTAL_UTILS.removeCookieHttps("pp_onbehalf");
		},
		isUserOnBehalf:function() {
			var isOnbehalf = this.getUserOnBehalf() != null;
			return isOnbehalf;
		},
		logout:function(logout_endpoint) {
			EC_PORTAL_UTILS.removeCookieHttps("pp_onbehalf");
			//EC_PORTAL_UTILS.removeCookieHttps("PORTAL_SESSION_ID");
			EC_PORTAL_UTILS.removeCookie("ec_status_local");
			EC_PORTAL_UTILS.redirect("/research/participants/api/authentication/logout.html?service=http://"+EC_PORTAL_UTILS.getHost()+logout_endpoint);
		},
		init:function() {
			EC_PORTAL_UTILS.log("begin init");
			this.serviceAvailable = false;
			this.loggedIn = false;

			// get the scope from the page (PRIVATE,PUBLIC or MIXED)
			browsingScope = EC_PORTAL_UTILS.getBrowsingScope();

			//get assumption for login state
			var localLoginState = this.getLocalCurrentLoginState();

			// unknow local login state,narrow it according to the scope of the page
			if(this.isLocalLoginStateUnknown()) {
				localLoginState = this.narrowLocalUnknownLoginState(browsingScope);
			}

			EC_PORTAL_UTILS.log("local login state:" + localLoginState);

			serviceAvailable = this.isServiceAvailable();
			if(serviceAvailable) {
				EC_PORTAL_UTILS.log("login service up");
				if(EC_PORTAL.LOGIN_STATUS_LOCAL.LOGGED_IN == localLoginState) {
					if(EC_PORTAL.utils.isProtocolHTTP()) {
						EC_PORTAL.utils.redirectCurrentURLToSSL();
					}
					var isLoggedIn = this.isUserLoggedIn();
					if(!isLoggedIn) {
						EC_PORTAL.utils.log("LOGGED IN:"+isLoggedIn)
						// do login
						this.login()
					}
					user = this.getUser();
			}

		EC_PORTAL_UTILS.log("end init");
		}
	}
};
})(window,jQuery);

/*******************************************************************************************************/
/*  Resources related functions
/*******************************************************************************************************/
//var ajax_array=
EC_PORTAL.namespace('EC_PORTAL.resources')

//var BASE_PATH:"https://ec.europa.eu/research/participants/secure/..."
EC_PORTAL.resources = (function() {
	var resources = [];
	var resources_public = [];
	var ENDPOINT = "enpoint";
	var PANEL_ID = "panel_id";
	var SCOPE = "scope";

	return {
		load: function(resourceEndpoint,panel_id) {
			var resource = [];
			resource[ENDPOINT] = resourceEndpoint;
			resource[PANEL_ID] = panel_id;
			resources.push(resource);
		},
		display: function(params) {
					numResources = resources.length;
					for(i =0;i < numResources;i++) {
						endpoint = resources[i][ENDPOINT];
						panel_id = resources[i][PANEL_ID];

						this.loadAndDisplay(endpoint,panel_id,params);
					}
					// to do
				},
				loadAndDisplay: function(resourceEndPoint,panel_id,params) {
					if(params === undefined || params == null){
						params = {};
					}
					var ajax;
					if(ajax) {
						ajax.abort();
					}
					EC_PORTAL_UTILS.log(panel_id);
					panel_id_pointer = "#"+panel_id;



					//$("body").append('<div id="spinner" style="position:fixed;top:50%;left:50%"></div>');
					//$("body").on('spin',function(event){$("#spinner").spin("small");});
					//$(panel_id_pointer).html("");
					//$("#spinner").trigger('spin');
					//ecSpinningStart($('body'));
					FEF.components.loadSpinnerEvents();
					FEF.components.initiateSpinning()

					ecSpinningStart($("body"));
					ajax = $.ajax({
								async:true,
								url:resourceEndPoint,
								method: 'GET',
								data:params,
								crossDomain: false,
								timeout:60000,
								cache: false,
								dataType:'html',
								xhrFields : {
									withCredentials: true
								} ,
								statusCode: {
									403: function() {
										//$(panel_id_pointer).html('<span class="error">Authentication is required</span>');
										$(panel_id_pointer).html('<span><div class="alert alert-error"><b>Authentication is required.</b></div></span>');										
									}
								},
								success: function(data){
									$(panel_id_pointer).html(data);
									 if(panel_id_pointer != "#search_organisations") {
										 FEF.components.initiateTables();
									 }
								},
								error : function(xhr,status,error){
									if(status==="timeout" || 404 == xhr.status || 503 == xhr.status) {
										$(panel_id_pointer).html('<span><div class="alert alert-error"><b>We are unable to provide you with the information requested at this moment due to a technical error. <br>Please try again later.</b></div></span>');										
									}
									else {
										$(panel_id_pointer).html('<span><div class="alert alert-error"><b>We are unable to provide you with the information requested at this moment due to a technical error. <br>Please try again later.</b></div></span>');										
									}
								},
								complete:function(data) {
									ecSpinningStop($("body"));
								//	$("#spinner").spin(false);
								$(".ec-spinning-spinner-wrapper").remove(); // JLE
								//ecSpinningStop($('body'));

								}

						});

				}
		};
})();

// export alias
var EC_PORTAL_RESOURCES = EC_PORTAL.resources;
