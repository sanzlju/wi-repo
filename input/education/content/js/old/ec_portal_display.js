jQuery.support.cors = true;
var authn = EC_PORTAL.authn;
EC_PORTAL.authn.init();
 
$(document).ready(function (){

    // control event
    controlEvent();

	if (EC_PORTAL.authn.isServiceAvailable()) {

		EC_PORTAL_UTILS.log("login service up:");
		$("body").removeClass("secure").addClass("anonymous").removeClass("admin").removeClass("on-behalf");

		isLoggedIn = authn.isUserLoggedIn();
		
		if (isLoggedIn) {
		
			$("body").removeClass("anonymous").addClass("secure");
			user = EC_PORTAL.authn.getUser();
			if (user.isAdmin) {$("#onbehalf_container").removeClass("anonymous").addClass("admin");}
			displayUserDetails(user);
			setUpAdmin(user);
			displayResources();

			$('#search_button').on('click',searchOrganisationsHandler);
			// click on the work as
			$('a.poplight[href^=#]').on('click',onBehalfWindowHandler);
			// click submit
			$('body').on("click","#imp_button",onBehalfSubmitHandler);
			// close popup
			$('body').on("click","a.close, #fade",closeOnBehalfPopup);

		}

	} else {

		$("body").removeClass("secure").addClass("anonymous");

	}

});

/*
/*
/* On behalf handlers/functions
/**************************************************************************/

function displayResources() {

	var params = {};
	ridTxt = $('head meta[name=portal-rid]').attr('content');
	var ridVal = EC_PORTAL.utils.getParameterByName(ridTxt);
	var isValidParam = EC_PORTAL.utils.isValidParam(ridVal);
	if (isValidParam) {$(".has_rid").on("click",{name:ridTxt,value:ridVal},resourceTabClick);params[ridTxt] = ridVal;}
	if(EC_PORTAL.authn.isUserOnBehalf()) {params["uid"] = EC_PORTAL.authn.getUserOnBehalf();}
	EC_PORTAL.resources.display(params);

}

function resourceTabClick(event) {

	event.preventDefault();
	name =event.data.name;
	value = event.data.value;
	window.location.href=$(this).attr("href")+"?"+name+"="+value;

}

function setUpAdmin(user){if(user.isAdmin) {$("body").addClass("admin");displayEffectiveUser();}}

function onBehalfWindowHandler() {

	if (EC_PORTAL.authn.isUserOnBehalf()) {

		EC_PORTAL.authn.removeUserOnBehalf();
		$('body').removeClass("on-behalf");
		EC_PORTAL.utils.reload();

	} else {

		openOnBehalfPopup();

	}

}

function onBehalfSubmitHandler(event) {

	var userId = $("#myid").val();
	$("#imp_popup_name").remove();
	EC_PORTAL.resources.display({"uid":userId});
	EC_PORTAL.authn.setUserOnBehalf(userId);
	closeOnBehalfPopup();
	EC_PORTAL.utils.reload();

}

function displayEffectiveUser() {


	user = authn.getUser();
	user = EC_PORTAL.authn.getUser();
	
	if( user.isAdmin) {

		uid = user.uid;
		$("body").addClass("admin");
		if(EC_PORTAL.authn.isUserOnBehalf()) {uid = EC_PORTAL.authn.getUserOnBehalf();$('body').addClass("on-behalf");}
		$(".pp-eid").text(uid);

	}
}

function displayUserDetails(user) {if (user.isLoggedIn()) {EC_PORTAL_UTILS.log(user);$(".pp-username").html(user.firstName + " " + user.lastName);}}

function searchOrganisationsHandler(event) {

	event.preventDefault();
	var paramsNames = ['searchTerm','country','city','vat','website']
	var params = {};

	for (i = 0; i < paramsNames.length;i++) {

		if(paramsNames[i] !== undefined) {

			var paramValue = $('#' + paramsNames[i]).val();
			if(paramValue !== undefined && paramValue != null && paramValue.trim() != "") {params[paramsNames[i]] = paramValue;}

		}

	}

	params[EC_PORTAL.i18n.getLangParameterName()] = EC_PORTAL.i18n.getLang();
	var searchEndpoint = $("#search_organisations_endpoint").text();

	if (searchEndpoint !== undefined) {EC_PORTAL.resources.loadAndDisplay(searchEndpoint,'search_organisations',params)}
}

function openOnBehalfPopup() {

	$("body").append('<div id="imp_popup_name" class="popup_block"><form id="imp_form" method="POST"><input type="text" id="myid" /><br /><input type="button" id="imp_button" value="Submit"/></form>	</div>');
	var popID = $("#onbehalf").attr('rel');
	var popWidth = 500;

	$('#' + popID).fadeIn().css({'width': Number(popWidth)}).prepend('<a href="#" class="close">Close</a>');

	// retrieve margin in order to center the window - add 80px in order to conform with css
	var popMargTop = ($('#' + popID).height() + 80) / 2;
	var popMargLeft = ($('#' + popID).width() + 80) / 2;

	$('#' + popID).css({'margin-top' : -popMargTop,	'margin-left' : -popMargLeft});

	// fade-in effect
	$('body').append('<div id="fade"></div>');
	$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();
	return false;

}

function closeOnBehalfPopup() {$('#fade , .popup_block').fadeOut(function() {$('#fade, a.close').remove();});return false;}

// JLE

var ROOT_PATH = document.URL.substring(0, document.URL.indexOf('desktop') + 10);

// ------------------------------------------ main operations -----------------------------------------------

// control event
function controlEvent() {

	// login event
	$('.pp-login').click(function(event) {

		event.preventDefault();
		
		if(!EC_PORTAL.authn.isServiceAvailable()){

			// server not available -> error message
			error('sorry');

		} else {

			EC_PORTAL.authn.login();

		}

	});

}

//------------------------------------------ util operations -----------------------------------------------

// error handler
function error(element) {

  if (element == "sorry") {

    $.ajax({url: ROOT_PATH + "/fragment/sorry.html", success: function (data) {$('.alert-error').remove();$('#content').prepend(data); $('#sorry').slideDown(); $('#sorry').delay(7000).fadeOut(1000);}, dataType: 'html'});

  } else {

    var fragment = element.attr('id');

    // html fragment
    if (fragment.indexOf(".html") != -1) {

      element.load(ROOT_PATH + "/fragment/missing.html");

    // dynamic fragment
    } else {

	  if (!$('#unavailable').length && !$('#sorry').length) {

	      $("#content").prepend($("<div>").load(ROOT_PATH + "/fragment/unavailable.html").html());

	  }

    }

  }

}
