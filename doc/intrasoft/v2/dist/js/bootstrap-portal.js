/* ===================================================
 * pp-bootstrap v 0.0.1
 * ===================================================
 * Participant Portal Bootstrap Extension
 * =================================================== */


/*
** Wizard Anchors
**		A anchors having a wizardHxW class will automatically open in
**		a new window as a wizard
**		Classes: wizard597x720 (urf) wizard800x1200 (sep)
*/
function openCenteredWindow(url, height, width, name, parms) {
   var left = Math.floor( (screen.width - width) / 2);
   var top = Math.floor( (screen.height - height) / 2);
   var winParms = "top=" + top + ",left=" + left + ",height=" + height + ",width=" + width;
   if (parms) { winParms += "," + parms; }
   var winName = name.replace(/ /g,"_");
	var win = window.open(url, winName, winParms);
   if (parseInt(navigator.appVersion) >= 4) { win.window.focus(); }
   return win;
}

$(function(){

	$("body").on("click", "a.wizard597x920" , function(event){event.preventDefault();openCenteredWindow($(this).attr('data-url'), 597, 920, $(this).text(),'scrollbars=1');});
	$("body").on("click", "a.wizard800x1200" , function(event){event.preventDefault();openCenteredWindow($(this).attr('data-url'), 800, 1200, $(this).text(),'scrollbars=1');});
	$("body").on("click", "a.wizard700x1000" , function(event){event.preventDefault();openCenteredWindow($(this).attr('data-url'), 700, 1000, $(this).text(),'scrollbars=1');});
    $("body").on('click', 'a.modal-window-button', function(event) {
        event.preventDefault();
        loading($('#modalWindowBody'));
        $('#modalWindowBody').load(site.rest + '/' + this.attributes['href'].value);
        $('#modalWindow').modal();
    });

});

$('.fragment').each(function(index) {
	var fragment_url = $(this).data('url');
	// If necessary, massage here the url string
	// e.g. if $(this).data('options').addpath == true
	// you may also check if fragment_url != ''
	// alert (urlExists(fragment_url));
	
	$(this).load(fragment_url);               	
		
});


/*
* Custom portal javascript
*/

/** 
	Main FE-Framework controller AMD module. <br />
	All the application is enclosed to the FEF modules.
	@module FEF
	*/
	
if(typeof(FEF)=="undefined") {
	var FEF={};
	(function (FEF) { }(FEF));
}

/**
* Portal Utilities 
*/

FEF.portal = (function(){
	
	/**
	* Handles the clicks on the my area menu
	* @method handleMyAreaClicks
	*/
	function handleMyAreaClicks() {
		
		
		$("#pp-myarea").click(function(e) {
			if(!$(this).hasClass('open') && $(this).hasClass('ec-fixed')) {
				//this should act as a fixed dropdown, without obstructing the content
				$('body').addClass('my-area-fixed');
			}
			if($(this).hasClass('open') && $(this).hasClass('ec-fixed')) {
				//this should act as a fixed dropdown, without obstructing the content
				$('body').removeClass('my-area-fixed');
			}
       });
		
	}
	
	/**
	* Shows and hides the filters on the FP7 area
	* @method handleFiltersContainer()
	**/
	
	function handleFiltersContainer() {
		
			$('#btn-filtering').unbind('click').click(function(e) {
				if(!$('#filterbox').hasClass("open")) $('#filterbox').slideDown(500,null,function(){ $('#filterbox').addClass("open") } );
				else $('#filterbox').slideUp(500,null,function(){ $('#filterbox').removeClass("open") });
            });
	}
	
	/*scope*/
	return {
		handleMyAreaClicks:handleMyAreaClicks,
		handleFiltersContainer:handleFiltersContainer
	}
}());


/**
* Custom components initialisation
*/
$(document).ready(function() {
	
	//initiate portal interactions
	FEF.portal.handleMyAreaClicks();
	FEF.portal.handleFiltersContainer();
	
	//initiates magic line
	FEF.components.initiateMagicLine();
	
	//initiates components
	FEF.components.initiateTables();
	FEF.components.initiateCarousel();
	FEF.components.loadTreeEvents();
	FEF.components.initiateEcTree();
	FEF.components.initiateAccordion();
	FEF.components.initiateTabNavigation();
	
} ); //end document ready