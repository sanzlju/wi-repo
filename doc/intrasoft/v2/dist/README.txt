This is the output folder structure for built HTML pages.
Structure is as follows:

/<target>|<support files>/<lang>
where
	target={desktop, basic, tablet, smartphone}
	support_files={js, css, img, docs}
	lang={en, fr, ...}

