YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "FEF.components",
        "FEF.data",
        "FEF.events",
        "FEF.utils"
    ],
    "modules": [
        "FEF"
    ],
    "allModules": [
        {
            "displayName": "FEF",
            "name": "FEF",
            "description": "Main FE-Framework controller AMD module. <br />\nAll the application is enclosed to the FEF modules."
        }
    ]
} };
});